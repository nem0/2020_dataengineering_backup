import random
import string
import json
import pymongo
from pymongo import MongoClient
from pprint import pprint
import requests
import credentials

user = credentials.credentials['user']
pwd = credentials.credentials['pwd']

connection_string = f"mongodb+srv://{user}:{pwd}@dataengineer-dv2d1." +\
"mongodb.net/test?retryWrites=true&w=majority"

# Change to the database 'dataengineer' and collection
# 'github_commits_{user}'. Both are not created until a first document is
# added.
client = MongoClient(connection_string)
db = client['dataengineer']
coll = db[f'playground_{user}']

def copy_db(db_name, collection_name):
    docs = client[db_name][collection_name].find()
    coll.insert_many(docs)
