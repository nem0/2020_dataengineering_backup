import io
import json
import time
import numpy as np

with open('tweet_dict.json', 'r') as f1:
    d = json.load(f1)
  
with open('words.json', 'r') as f2:
    words = json.load(f2)

def generate_tweet():
    first_word = np.random.choice(words)
    tweet_length = 30
    markow_chain = [first_word]
    for i in range(tweet_length):
        last_word = markow_chain[-1]
        next_word = np.random.choice(d[last_word])
        markow_chain.append(next_word)
    return ' '.join(markow_chain)
    
def start():
    for i in range(1000):
        with io.open(f'tweets/{str(i).zfill(4)}.txt', 'w', encoding='utf-8') as f3:
            random_tweet = generate_tweet()
            f3.write(random_tweet)
        time.sleep(1)
 