import socket
import sys
import requests
import requests_oauthlib
import json
import twitter_token
from pyspark.streaming import StreamingContext

my_auth = requests_oauthlib.OAuth1(
    twitter_token.CONSUMER_KEY,
    twitter_token.CONSUMER_SECRET,
    twitter_token.ACCESS_TOKEN,
    twitter_token.ACCESS_SECRET,
)

geo_locations = [
    10, # longitude start luebeck
    53, # latitude start luebeck
    11, # longitude end luebeck
    54, # latitude end luebeck
    10, # longitude start berlin
    51, # latitude start berlin
    16, # longitude end berlin
    57, # latitude end berlin
]

locations = ','.join([str(loc) for loc in geo_locations])

def send_tweets_to_spark(http_resp, tcp_connection):
    for line in http_resp.iter_lines(decode_unicode=True):
        if len(line) == 0:
            continue
        try:
            full_tweet = json.loads(line)
            if 'text' in full_tweet:
                tweet_text = full_tweet['text'] + '\n'
                tcp_connection.send(tweet_text.encode('utf-8'))
        except Exception as e:
            print(f"Error in send_tweets_to_spark: {e}")
            print('Message:', line)

def print_tweets(http_resp):
    for line in http_resp.iter_lines():
        try:
            full_tweet = json.loads(line)
            print(json.dumps(full_tweet, indent=2))
            print ("######################################")
        except Exception as e:
            #print(f"Error in print_tweets: {e}")
            pass

def get_tweets():
    url = 'https://stream.twitter.com/1.1/statuses/filter.json'
    query_data = [('locations', locations), ('track', '#')]
    query_url = url + '?' + '&'.join([str(t[0]) + '=' + str(t[1]) for t in query_data])
    response = requests.get(query_url, auth=my_auth, stream=True)
    print(query_url, response)
    return response


def cleanDataStream(dataStream):
    #split words and clean unwanted characters
    returnValue = dataStream\
    .flatMap(lambda line: line.split(' '))\
    .flatMap(lambda line: line.split('-'))\
    .flatMap(lambda line: line.split(','))\
    .filter(lambda line: line != '')
    
    #print(type(returnValue))
    #print(type(dataStream))
    return returnValue
    #return dataStream

def updateFunction(newValues, runningCount):
    if runningCount is None:
        runningCount = 0
    return sum(newValues, runningCount)  # add the new values with the previous running count to get the new count

    
#def findString(dataStream):
    
    
def start():
    TCP_IP = "localhost"
    TCP_PORT = 9009
    conn = None
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.listen(1)
    print("Waiting for TCP connection...")
    conn, addr = s.accept()
    print("Connected... Starting getting tweets.")
    resp = get_tweets()
    send_tweets_to_spark(resp, conn)
