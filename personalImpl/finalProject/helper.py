#helper file
from pathlib import Path
#import time
#import threading
import concurrent.futures
import requests
from PIL import Image
import shutil
import os
import queue
import re
import time
import threading

numberOfSearchPages = 200
sleepBeforeNewSearchRound_seconds = numberOfSearchPages * 10 #assume it takes less than 5 seconds to scrap single page

def pruneListOfLinks(inputList):
    #all entries are duplicated, need to reduce to single
    #https://www.w3schools.com/python/trypython.asp?filename=demo_howto_remove_duplicates
    inputList = list(dict.fromkeys(inputList))
    #print(len(inputList))
    
    #TODO: number of desired hits per page is varying by page (sometimes 41 sometimes 49). Need to purge suggested images to purchase, and the adverts for other products (such as phone, air vaporizier etc.) 
    #in clean implementation need to search by sections of html page (one section will contain listed products, other will do adverst etc.)
    #going ahead with taking only first 35 hits per page as they are guaranteed to be of interest
    inputList = inputList[0:35]
    #print(len(inputList))
    
    #postprocess string to remove quotation marks [1:-2]
    for i, _ in enumerate(inputList):
        inputList[i] = inputList[i][1:-1]
    
    return inputList
  

#from other project to download and manipulate image objects
def create_dir(dir):
    Path(dir).mkdir(parents=True, exist_ok=True)
    
def download_image(name, url):
    print(name)
    print(url)
    ib = requests.get(url).content
    filename = f'extractedImages/{name}.jpg'
    create_dir('extractedImages')
    with open(filename, 'wb') as f:
        f.write(ib)
    #print(f"Done downloading {name}'s file.")
    return (filename, f'{name}.jpg')

def download_image_png_andConvertToJpg(name, url):
    print('entered download image png')
    print(name)
    print(url)
    ib = requests.get(url).content
    filename = f'extractedImages/{name}.png'
    create_dir('extractedImages')
    with open(filename, 'wb') as f:
        f.write(ib)
    #print(f"Done downloading {name}'s file.")
    
    print('now change image')
    #https://stackoverflow.com/questions/43258461/convert-png-to-jpeg-using-pillow
    im = Image.open(filename)
    rgb_im = im.convert('RGB')
    filenameJpg = f'extractedImages/{name}.jpg'
    rgb_im.save(filenameJpg)
    
    print('after changing')
    print(filenameJpg, filename, f'{name}.jpg')
    return (filenameJpg, f'{name}.jpg')

def transform_image(filepathExtracted, imageName):
    im = Image.open(filepathExtracted)
    
    #rotate portrait to ensure all have panorama outlin
    if (im.size[0] < im.size[1]):
        im = im.rotate(90, expand=True)

    #change resolution
    verticalSize = 300
    horizontalSize = 600
    #TODO exploratory data analysis on downloaded images to understand what is the best size/aspect ratio for images.
    size = (horizontalSize, verticalSize)
    im2 = im.resize(size)

    #save transformed file
    create_dir('transformedImages')
    filepathTransformed = f'transformedImages/{imageName}'
    im2.save(filepathTransformed)
    return filepathTransformed


def purgeAllDBEntriesAndFiles(session):
    #remove folder
    if os.path.isdir('transformedImages/'):
        shutil.rmtree('transformedImages/') 
        print('removed transformedImages')
    if os.path.isdir('extractedImages/'):
        shutil.rmtree('extractedImages/') 
        print('removed extractedImages')
    #remove DB entries
    deleteEntireDb(session)
    
 
#download search results eternally
def downloadSearchResults(searchQueue):
    while True: #eternal loop
    #downloads all pages into a queue
#urlOriginPage= 'https://www.ebay.de/b/Kunstlerische-Malerei/551/bn_2397987?_pgn=13'
#with below url format items are ordered (latest entries are first found)
        urlPreIndex = 'https://www.ebay.de/b/Kunstlerische-Malerei/551/bn_2397987?_pgn='
        urlMaxIndex=numberOfSearchPages #seems around 200 of these indices exist
        urlPostIndex = '&_sop=10'

        #urlMaxIndex=20
        for i in range(0, urlMaxIndex):
            print(f'downloading {i}. page with search results')
            urlOriginPage = urlPreIndex+str(i)+urlPostIndex
            searchQueue.put((urlOriginPage, requests.get(urlOriginPage).content))
    
        sleepIntervalSeconds = sleepBeforeNewSearchRound_seconds
        #sleepIntervalSeconds = 0
        time.sleep(sleepIntervalSeconds)
        print('downloadSearchResults: sleep done, starting new round of search page result fetching')

def fetchElementFromSearchPage(searchQueue, elementQueue):
    while True: #eternal loop
        urlOriginPage, resultsPage = searchQueue.get()
        searchQueue.task_done()
        #Regular expression compile (identify strings starting with href and ending with >< as end of html object
        #(?<=href=) precedes string of importance
        #.*?, where ? enforces non-greedy .*
        #(?=><) postcedes string of importance
        regexPage= re.compile(r"(?<=href=)\"https://www.ebay.de/itm.*?(?=\><)", re.MULTILINE)

        #apply regexp to fetch results
        httpsHits = re.findall(regexPage, str(resultsPage))
        #print(len(httpsHits))

        httpsHits = pruneListOfLinks(httpsHits)

        for productUrl in httpsHits:
            regexName = re.compile(r"(?<=ebay.de/itm/).*?(?=/)", re.MULTILINE)
            productName = re.findall(regexName, productUrl)[0]

            productPage = requests.get(productUrl).content
    
            print(f'finished extracting URL from {urlOriginPage} \n found {len(httpsHits)} entries \n last entry is {productName} \n and its link is {productUrl}')
    

            elementQueue.put((productName, productUrl, productPage))
    
#to fetch image URL from the product page
def fetchImageURL(page):
    #identify image link
    regexImage= re.compile(r"(?<=itemprop=\"image\" src=)\"https://i.ebayimg.com/images/.*?(?= style=)", re.MULTILINE)
    httpsHitsImage = re.findall(regexImage, str(page))
    #TODO: can use maxImageUrl to extract images with max resolution instead of the easy solution above

    #print(len(httpsHitsImage))
    httpsHitsImage = pruneListOfLinks(httpsHitsImage)
    #print(len(httpsHitsImage))
    if (httpsHitsImage[0] != None):
        print(f'image path is {httpsHitsImage[0]}')
        return httpsHitsImage[0]
    else:
        print('fetchImageURL error no image found')

def fetchPrice(page):
    #fetch price using itemprop="price" from productPage
    productPage = page
    #print(productPage)
    #productPage = requests.get('https://www.ebay.de/itm/BURGSTALLER-Gesichter-Wandbilder-Malerei-abstrakt-Kunst-Gemalde-Unikat-Acrylbild/283861215133?hash=item421772679d:g:~EEAAOSwiuVep7V6').content

    regexPrice= re.compile(r"(?<=itemprop=\"price\"  style=\"\" content=).*?(?=>)", re.MULTILINE)
    httpsHitsPrice = re.findall(regexPrice, str(page))
    print(httpsHitsPrice)
    #TODO below is workaround, there might be different styles for price, need to find better regexp
    if httpsHitsPrice == []:
        regexPrice= re.compile(r"(?<=itemprop=\"price\" content=).*?(?=>)", re.MULTILINE)
        httpsHitsPrice = re.findall(regexPrice, str(page))
    if httpsHitsPrice == []:
        regexPrice= re.compile(r"(?<=itemprop=\"price\" style=\"\" content=).*?(?=>)", re.MULTILINE)
        httpsHitsPrice = re.findall(regexPrice, str(page))
    #print(httpsHitsPrice)
    #below special case when UR also present in price
    httpsHitsPrice = re.sub('UR ','', httpsHitsPrice[0])
    #print(httpsHitsPrice)
    #print('test')
    httpsHitsPrice = re.sub(',','.', httpsHitsPrice)
    #print(httpsHitsPrice)
    #print(httpsHitsPrice[1:-1])
    productPrice = float(httpsHitsPrice[1:-1])
    #print(f'image price is {productPrice}')
    #print(len(httpsHitsImage))
    #print(httpsHitsPrice[0])
    #print(type(httpsHitsPrice[0]))
    return productPrice

#to fetch all the information from the product page 
def fetchElementsFromProductPage(elementQueue, sqlInputQueue):
    while True: #eternal loop
        page = elementQueue.get()
        elementQueue.task_done()

        #fetching elements
        elementName = page[0]
        elementUrl = page[1]
        pageContent = page[2]
    
        print(f'fetching element {elementName} from {elementUrl}')
        imageUrl = fetchImageURL(pageContent)
        price = fetchPrice(pageContent)
        
        if ((imageUrl!=None) and (price!=None)):
            #transform and save elements
            
            #check if image is png
            regexpng= re.compile(r".*?(?=\.png)", re.MULTILINE)
            isPng = re.findall(regexpng, str(imageUrl))
            
            if (isPng != []):
                print(f'found png image that requires special handling')
                (filepathExtracted, imageName) = download_image_png_andConvertToJpg(elementName, imageUrl)
            else:
                (filepathExtracted, imageName) = download_image(elementName,imageUrl)#also saves inter-product in extractImages

            print(f'filepathExtracted {filepathExtracted} imageName {imageName}')

            filepathTransformed = transform_image(filepathExtracted, imageName)
    
            sqlInputQueue.put((elementName, price, filepathExtracted, filepathTransformed))
    
    
def fillSQLDb(sqlInputDB, session):
    while True: #eternal loop
        entryInDB = sqlInputDB.get()
        sqlInputDB.task_done()
        #sqlInputDB[0], productPrice, filepathExtracted, filepathTransformed, session
        sqlNewEntry(*entryInDB, session)
    
#print how many elements in Q
def printQueueSize(queue):
    while True:
        print(searchQueue.qsize())
        time.sleep(5)
#extract URLs inside page
#def extractUrlFromSearchPage(inputQueue):
    

##SQL
from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

#engine = create_engine('sqlite:///recommenderSystem.db', echo=True)
engine = create_engine('sqlite:///recommenderSystem.db', echo=False)
Base = declarative_base()


class RecommenderEntry(Base):
    __tablename__ = "recommenderEntryTable"
 
    id = Column(Integer, primary_key=True)
    name = Column(String)
    price = Column(Float)
    locExtracted = Column(String)
    locTransformed = Column(String)

    def __init__(self, name, price, locExtracted, locTransformed):
        self.name = name
        self.price = price
        self.locExtracted = locExtracted
        self.locTransformed = locTransformed

Base.metadata.create_all(engine)

#searches for database across name
#TOOD: can also do price comparison. Potentially also image comparison (convolution) with other python plugins
def sqlCheckEntryExists(imageName, price, filepathExtracted, filepathTransformed, session):
    print(f'checking if entry {imageName} exists ')
    found = False
    for entry in session.query(RecommenderEntry):
        if (entry.name == imageName):
            found = True   
    #print(found)
    return found

def sqlNewEntry(imageName, price, filepathExtracted, filepathTransformed, session):
    #print('start sqlNewEntry')
    if (sqlCheckEntryExists(imageName, price, filepathExtracted, filepathTransformed, session) == False):
        print('input new element into DB')
        entry = RecommenderEntry(imageName, price, filepathExtracted, filepathTransformed)
        session.add(entry)
        session.commit()

        
def printAllDb(session):
    print('printing entire database content\n\n')
    for entry in session.query(RecommenderEntry):
        print(f'{entry.name} {entry.price} {entry.locExtracted} {entry.locTransformed} \n')
        
        
def deleteEntireDb(session):
    session.query(RecommenderEntry).delete()
    session.commit()
