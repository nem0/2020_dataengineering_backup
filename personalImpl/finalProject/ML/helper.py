##SQL
from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

from PIL import Image
import numpy as np

#engine = create_engine('sqlite:///recommenderSystem.db', echo=True)
engine = create_engine('sqlite:///recommenderSystem.db', echo=False)
Base = declarative_base()


class RecommenderEntry(Base):
    __tablename__ = "recommenderEntryTable"
 
    id = Column(Integer, primary_key=True)
    name = Column(String)
    price = Column(Float)
    locExtracted = Column(String)
    locTransformed = Column(String)

    def __init__(self, name, price, locExtracted, locTransformed):
        self.name = name
        self.price = price
        self.locExtracted = locExtracted
        self.locTransformed = locTransformed

Base.metadata.create_all(engine)

#searches for database across name
#TOOD: can also do price comparison. Potentially also image comparison (convolution) with other python plugins
def sqlCheckEntryExists(imageName, price, filepathExtracted, filepathTransformed, session):
    print(f'checking if entry {imageName} exists ')
    found = False
    for entry in session.query(RecommenderEntry):
        if (entry.name == imageName):
            found = True   
    #print(found)
    return found

def sqlNewEntry(imageName, price, filepathExtracted, filepathTransformed, session):
    #print('start sqlNewEntry')
    if (sqlCheckEntryExists(imageName, price, filepathExtracted, filepathTransformed, session) == False):
        print('input new element into DB')
        entry = RecommenderEntry(imageName, price, filepathExtracted, filepathTransformed)
        session.add(entry)
        session.commit()

        
def printAllDb(session):
    print('printing entire database content\n\n')
    for entry in session.query(RecommenderEntry):
        print(f'{entry.name} {entry.price} {entry.locExtracted} {entry.locTransformed} \n')
        

def deleteEntireDb(session):
    session.query(RecommenderEntry).delete()
    session.commit()

def sql_fetchImage(session):
    temp = session.query(RecommenderEntry)
    #print(type(temp))
    return temp.first().locTransformed
    
    #convert image file into RGB

def convertJpgToRGBArray(filepath):
    #import skimage.color as SKI
    img = Image.open(filepath)
    
    img = img.convert('RGB') #to make it work also for BW pictures
#    print(np.shape(img))
    
#    print(np.shape())
#    print('converted')
#'transformedImages/Olgemalde-Landschaft-Natur-Flusslauf-Bild-Signiert-Tillio-Francalanci.jpg ')

    #SKI.color.gray2rgb()s
    
#    print(SKI.gray2rgb(img))
    array = np.array(img) #first dimension is height, second is width, third are channels
    #special : if picture B&W there are no channels as they are all 0. 
    #TODO: What happens if any individual channel is 0 ?
    
    #print(array.shape)
    
    return array

def getExampleImageDimensions(session):
    #to fetch the dimensions for numpy array
    image = sql_fetchImage(session)
    imageAsArray = convertJpgToRGBArray(image)
    return np.shape(imageAsArray)


def countElemsInDb(session):
    numberElems = 0
    for entry in session.query(RecommenderEntry):
        numberElems+=1
    
    return numberElems

def listFromTuple(inputTuple):
    someList=[]
    for i in inputTuple:
 #   print(i)
        someList.append(i)
    return someList
    
def extractTrainingData(session, X_train, Y_train):
    print('entered extractTrainingData\n\n')
    #print(np.shape(X_train))
    #print(np.shape(Y_train))
    counter = 0
    for entry in session.query(RecommenderEntry):
        #save input data
        #print(counter)
        #print(entry.locTransformed)
        #X_train.append(convertJpgToRGB(entry.locTransformed))
        #print(np.shape(convertJpgToRGBArray(entry.locTransformed)))
        X_train[counter] = convertJpgToRGBArray(entry.locTransformed)
        #print(len(X_train))
        #print(np.shape(X_train))
        #save labels
        #Y_train.append(entry.price)
        Y_train[counter] = entry.price
        counter += 1