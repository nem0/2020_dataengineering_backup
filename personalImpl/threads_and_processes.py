import time
import threading
import concurrent.futures
import requests
from PIL import Image as im
import queue
import re #regexp



def get_urls():
    return {
        'Manu1': 'https://wallpaperaccess.com/download/4k-ultra-hd-galaxy-250472',
        'Manu2': 'https://effigis.com/wp-content/uploads/2015/02/DigitalGlobe_QuickBird_60cm_8bit_RGB_DRA_Boulder_2005JUL04_8bits_sub_r_1.jpg',
        'Christian': 'https://bollerkopp.net/wp-content/uploads/2017/03/wenn-die-kroeten-wandern.jpg',
        'Fabian1': 'https://corona-renderer.com/forum/index.php?action=dlattach;topic=8653.0;attach=33277;image',
        'Martin G.': 'https://i.redd.it/yd2ywv5rfbv21.jpg',
        'Martin K.': 'http://getwallpapers.com/wallpaper/full/2/7/0/75000.jpg',
        'Nemanja': 'https://cdn.eso.org/images/large/eso1907a.jpg',
        'Jan': 'https://wallpaperaccess.com//full/2207694.jpg',
        'Glafira': 'https://www.esa.int/var/esa/storage/images/esa_multimedia/images/2004/01/nearby_galaxy_ngc_1569_is_a_hotbed_of_star_birth_activity2/17880180-2-eng-GB/Nearby_galaxy_NGC_1569_is_a_hotbed_of_star_birth_activity.jpg',
        'Fabian2': 'http://paulandliz.org/Galaxies/Ngc/N891_210814.jpg',
        'Olaf': 'https://images.unsplash.com/photo-1569974507005-6dc61f97fb5c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80',
        'Joan': 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTM7zN2ko2J8AhbeQvEBlzLf4o6qblhBN5tdf23-u9b1wzTvCkE&usqp=CAU',
    }

def download_image(name, url):
    ib = requests.get(url).content
    filename = f'images/{name}.jpg'
    with open(filename, 'wb') as f:
        f.write(ib)
    print(f"Done downloading {name}'s file.")
    return filename

def fib(n):
    if n < 2:
        return n
    return fib(n-1) + fib(n-2)

def download_parallel(urls):
    with concurrent.futures.ProcessPoolExecutor() as ex:
        for name, url in urls.items():
            ex.submit(download_image, name, url)

def fib_parallel(ns=[32] * 20):
    with concurrent.futures.ProcessPoolExecutor() as ex:
        for n in ns:
            ex.submit(fib, n)

def timeit(method):
    def timed(*args, **kw):
        start = time.perf_counter()
        result = method(*args, **kw)
        end = time.perf_counter()
        
        duration = round(end - start, 3)
        thread_name = threading.currentThread().getName()
        print(f'Script {method.__name__} took {duration} second(s).')
        return result
    return timed


##queues
#@timeit
def processor(process_queue, final_queue):
    #global process_queue
    #global final_queue
    while True:
        pass
        # Hier implementieren als Zusatzaufgabe:
        # Bild verkleiner, Bild rotieren, Bild mit Filter bearbeiten
        if process_queue.empty():
            break

        pathToImage = process_queue.get()
        image = im.open(pathToImage)
        process_queue.task_done()
        #print(link)
        rotated = image.rotate(angle=45)
        
        #save file
        outFileName = re.sub('images/', '', pathToImage)
        outFileName2 = re.sub('\.jpg', '.png', outFileName)
        pathToFilename = f'processedImages/{outFileName2}'
        rotated.save(pathToFilename)
        print(f'rotated image stored at {pathToFilename}')
        final_queue.put(pathToFilename)
        # Mit Python Image Library (PIL)
        # Pfad zum bearbeiteten Bild in die final_queue tun.